In this project I have implemented the logic of building a **RecyclerView adapter with multiple view items** using delegate adapters.
You can see an example of how the application works at the **YouTube link:** https://youtu.be/teVU00Llfcc

---

Base classes: AdapterItemContract, AdapterContract

```
interface AdapterItemContract {
    fun id(): Any
    fun content(): Any
    fun updateData(newData: String) {}
}
```

```
abstract class AdapterContract<MODEL, in VH : RecyclerView.ViewHolder>(val modelClass: Class<out MODEL>) {
    abstract fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    abstract fun bindViewHolder(model: MODEL, viewHolder: VH)
}
```

---


