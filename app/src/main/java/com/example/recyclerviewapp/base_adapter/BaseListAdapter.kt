package com.example.recyclerviewapp.base_adapter

import android.util.SparseArray
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class BaseListAdapter(
    private val delegates: SparseArray<AdapterContract<AdapterItemContract, RecyclerView.ViewHolder>>
) : ListAdapter<AdapterItemContract, RecyclerView.ViewHolder>(ItemDiffCallback()) {

    override fun getItemViewType(position: Int): Int {
        for (i in 0 until delegates.size()) {
            if (delegates[i].modelClass == getItem(position).javaClass) {
                return delegates.keyAt(i)
            }
        }
        throw IllegalArgumentException("Can not get viewType for position $position")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        delegates[viewType].createViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val delegateAdapter = delegates[getItemViewType(position)]
        delegateAdapter.bindViewHolder(getItem(position), holder)
    }

    class Builder {

        private var count: Int = 0
        private val delegates: SparseArray<AdapterContract<AdapterItemContract, RecyclerView.ViewHolder>> = SparseArray()

        fun add(adapterContractContract: AdapterContract<out AdapterItemContract, *>): Builder {
            delegates.put(count++, adapterContractContract as AdapterContract<AdapterItemContract, RecyclerView.ViewHolder>)
            return this
        }

        fun build(): BaseListAdapter {
            require(count != 0) { "Register at least one adapter" }
            return BaseListAdapter(delegates)
        }
    }
}