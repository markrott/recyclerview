package com.example.recyclerviewapp.base_adapter

interface AdapterItemContract {

    fun id(): Any

    fun content(): Any

    fun updateData(newData: String) {}
}