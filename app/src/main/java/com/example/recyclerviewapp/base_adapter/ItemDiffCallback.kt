package com.example.recyclerviewapp.base_adapter

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

class ItemDiffCallback : DiffUtil.ItemCallback<AdapterItemContract>() {

    override fun areItemsTheSame(
        oldItem: AdapterItemContract,
        newItem: AdapterItemContract
    ): Boolean = oldItem::class == newItem::class && oldItem.id() == newItem.id()

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(
        oldItem: AdapterItemContract,
        newItem: AdapterItemContract
    ): Boolean = oldItem.content() == newItem.content()

}