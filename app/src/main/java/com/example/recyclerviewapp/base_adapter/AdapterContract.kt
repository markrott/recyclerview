package com.example.recyclerviewapp.base_adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class AdapterContract<MODEL, in VH : RecyclerView.ViewHolder>(val modelClass: Class<out MODEL>) {

    abstract fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    abstract fun bindViewHolder(model: MODEL, viewHolder: VH)
}