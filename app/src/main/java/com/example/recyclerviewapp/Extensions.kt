package com.example.recyclerviewapp

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.makeText
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun Context.showToast(msg: String){
    val toast = makeText(this, msg, Toast.LENGTH_SHORT)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}

fun Fragment.snack(message: String, length: Int = Snackbar.LENGTH_SHORT) {
    val snack = this.view?.let { Snackbar.make(it, message, length) }
    snack?.show()
}
