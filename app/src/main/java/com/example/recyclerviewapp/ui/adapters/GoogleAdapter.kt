package com.example.recyclerviewapp.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewapp.R
import com.example.recyclerviewapp.base_adapter.AdapterContract
import com.example.recyclerviewapp.inflate
import com.example.recyclerviewapp.ui.click.TrademarkClickData
import com.example.recyclerviewapp.ui.click.TrademarkItemClick
import com.example.recyclerviewapp.ui.models.GoogleModel
import kotlinx.android.synthetic.main.item_google.view.*

class GoogleAdapter(
    private val clickAction: TrademarkItemClick
) : AdapterContract<GoogleModel, GoogleAdapter.GoogleViewHolder>(GoogleModel::class.java) {

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.item_google)
        return GoogleViewHolder(view)
    }

    override fun bindViewHolder(model: GoogleModel, viewHolder: GoogleViewHolder) {
        viewHolder.bind(model)
    }

    inner class GoogleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(model: GoogleModel) {
            itemView.tv_google_token.text = model.token
            itemView.setOnClickListener { clickAction(TrademarkClickData.ItemIfo(model.token)) }
            itemView.btn_update_token.setOnClickListener { clickAction(TrademarkClickData.UpdateToken(adapterPosition)) }
        }
    }
}