package com.example.recyclerviewapp.ui.click

sealed class TrademarkClickData {
    class ItemIfo(val info: String) : TrademarkClickData()
    class UpdateToken(val position: Int) : TrademarkClickData()
}