package com.example.recyclerviewapp.ui.models

import com.example.recyclerviewapp.base_adapter.AdapterItemContract

data class GoogleModel(var token: String) : AdapterItemContract {

    override fun id(): Any = token

    override fun content(): Any  = token

    override fun updateData(newData: String) { token = newData }
}