package com.example.recyclerviewapp.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewapp.R
import com.example.recyclerviewapp.base_adapter.AdapterContract
import com.example.recyclerviewapp.inflate
import com.example.recyclerviewapp.ui.click.TrademarkClickData
import com.example.recyclerviewapp.ui.click.TrademarkItemClick
import com.example.recyclerviewapp.ui.models.FacebookModel
import kotlinx.android.synthetic.main.item_facebook.view.*
import kotlinx.android.synthetic.main.item_facebook.view.btn_update_token

class FacebookAdapter(
    private val clickAction: TrademarkItemClick
) : AdapterContract<FacebookModel, FacebookAdapter.FacebookModelViewHolder>(FacebookModel::class.java) {

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.item_facebook)
        return FacebookModelViewHolder(view)
    }

    override fun bindViewHolder(model: FacebookModel, viewHolder: FacebookModelViewHolder) {
        viewHolder.bind(model)
    }

    inner class FacebookModelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(model: FacebookModel) {
            itemView.tv_fb_token.text = model.token
            itemView.setOnClickListener { clickAction(TrademarkClickData.ItemIfo(model.token)) }
            itemView.btn_update_token.setOnClickListener { clickAction(TrademarkClickData.UpdateToken(adapterPosition)) }
        }
    }
}