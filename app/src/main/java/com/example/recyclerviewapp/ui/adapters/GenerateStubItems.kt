package com.example.recyclerviewapp.ui.adapters

import com.example.recyclerviewapp.base_adapter.AdapterItemContract
import com.example.recyclerviewapp.ui.models.FacebookModel
import com.example.recyclerviewapp.ui.models.GoogleModel
import com.example.recyclerviewapp.ui.models.TwitterModel
import java.util.*
import kotlin.random.Random

object GenerateStubItems {

    fun getItems(): List<AdapterItemContract> {
        val items = mutableListOf<AdapterItemContract>()
        items.add(GoogleModel(UUID.randomUUID().toString()))
        items.add(FacebookModel(UUID.randomUUID().toString()))
        items.add(TwitterModel(UUID.randomUUID().toString()))

        for (i in 1..10) {
            val uniqueID = UUID.randomUUID().toString()
            when (Random.nextInt(1, 150_000)) {
                in 1..50000 -> items.add(GoogleModel(uniqueID))
                in 50001..100_000 -> items.add(FacebookModel(uniqueID))
                in 100_001..150_000 -> items.add(TwitterModel(uniqueID))
            }
        }
        return items
    }
}