package com.example.recyclerviewapp.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewapp.R
import com.example.recyclerviewapp.base_adapter.AdapterContract
import com.example.recyclerviewapp.inflate
import com.example.recyclerviewapp.ui.click.TrademarkClickData
import com.example.recyclerviewapp.ui.click.TrademarkItemClick
import com.example.recyclerviewapp.ui.models.TwitterModel
import kotlinx.android.synthetic.main.item_twitter.view.*

class TwitterAdapter(
    private val clickAction: TrademarkItemClick
) : AdapterContract<TwitterModel, TwitterAdapter.TwitterViewHolder>(TwitterModel::class.java) {

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.item_twitter)
        return TwitterViewHolder(view)
    }

    override fun bindViewHolder(model: TwitterModel, viewHolder: TwitterViewHolder) {
        viewHolder.bind(model)
    }

    inner class TwitterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(model: TwitterModel) {
            itemView.tv_twitter_token.text = model.token
            itemView.setOnClickListener { clickAction(TrademarkClickData.ItemIfo(model.token)) }
        }
    }
}