package com.example.recyclerviewapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.recyclerviewapp.R
import com.example.recyclerviewapp.base_adapter.BaseListAdapter
import com.example.recyclerviewapp.snack
import com.example.recyclerviewapp.ui.adapters.*
import com.example.recyclerviewapp.ui.click.TrademarkClickData
import com.example.recyclerviewapp.ui.click.TrademarkItemClick
import kotlinx.android.synthetic.main.frg_trademark.*
import java.util.*

class TrademarkFragment : Fragment() {

    private val trademarkAdapter by lazy {
        BaseListAdapter.Builder()
            .add(GoogleAdapter(trademarkItemClick))
            .add(FacebookAdapter(trademarkItemClick))
            .add(TwitterAdapter(trademarkItemClick))
            .build()
    }

    private val trademarkItemClick: TrademarkItemClick = { clickData ->
        when (clickData) {
            is TrademarkClickData.ItemIfo -> snack(clickData.info)
            is TrademarkClickData.UpdateToken -> {
                updateToken(clickData)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_trademark, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rcv_trademark.adapter = trademarkAdapter

        val startItems = GenerateStubItems.getItems()
        trademarkAdapter.submitList(startItems)
    }

    private fun updateToken(clickData: TrademarkClickData.UpdateToken) {
        val selectedItem = trademarkAdapter.currentList[clickData.position]
        selectedItem.updateData(UUID.randomUUID().toString())
        trademarkAdapter.notifyItemChanged(clickData.position)
    }
}